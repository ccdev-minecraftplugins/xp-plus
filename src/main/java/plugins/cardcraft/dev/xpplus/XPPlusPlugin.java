package plugins.cardcraft.dev.xpplus;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import plugins.cardcraft.dev.xpplus.commands.BottleXPCommand;
import plugins.cardcraft.dev.xpplus.listeners.XPPlusListener;
import plugins.cardcraft.dev.xpplus.util.Logger;
import plugins.cardcraft.dev.xpplus.util.Utils;


public class XPPlusPlugin extends JavaPlugin {
	
    @Override
    public void onEnable() 
    {  	
    	saveDefaultConfig();
    	
    	if(this.getConfig().contains("debug-logging"))
    	{
    		Utils.debugLogging = this.getConfig().getBoolean("debug-logging");
    	}
    	
    	Logger.info("########################################");
    	Logger.info("#    XPPlus Plugin by cardcraft.dev    #");
    	Logger.info("########################################");
    	Logger.info("# DEBUG LOGGING     : "+Utils.debugLogging);
    	Logger.info("########################################");
    	
    	//getCommand("snowbomb").setTabCompleter(new SnowBombCommandTabCompleter());
    	getCommand("bottle").setExecutor(new BottleXPCommand(this));
        Bukkit.getPluginManager().registerEvents((Listener)new XPPlusListener(this), (Plugin)this);

    }  
}
