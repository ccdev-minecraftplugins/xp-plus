package plugins.cardcraft.dev.xpplus.util;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import plugins.cardcraft.dev.xpplus.XPPlusPlugin;

public class Utils {
	
    public static final int METRICS_ID = 9152;
    public static boolean debugLogging = false;

    public static void sendMessage(CommandSender sender, String message) {
        sender.sendMessage(colorize(message));
    }
    
    public static void sendMessage(Player player, String message) {
        player.sendMessage(colorize(message)); 
    }

    public static String colorize(String string) {
        return ChatColor.translateAlternateColorCodes('&', ChatColor.GRAY + string);
    }
    
    public static void debugLog(String inputString)
    {
    	if(debugLogging)
    	{
    		Logger.debug(inputString);
    	}
    }
}
