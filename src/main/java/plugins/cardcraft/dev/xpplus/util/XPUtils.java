package plugins.cardcraft.dev.xpplus.util;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;


public class XPUtils 
{
	//Calculates a player's total exp based on level and progress to next.
	public static int getExp(Player player) 
	{
		return getExpFromLevel(player.getLevel()) + Math.round(getExpToNext(player.getLevel()) * player.getExp());
	}
	
	public static int getLevel(Player player)
	{
		return player.getLevel();
	}
	
    // Calculate amount of EXP needed to level up
	public static int getExpFromLevel(int level) 
	{
		if (level > 30) 
		{
			return (int) (4.5 * level * level - 162.5 * level + 2220);
		}
		if (level > 15) 
		{
			return (int) (2.5 * level * level - 40.5 * level + 360);
		}
		return level * level + 6 * level;
	}

	// Calculates level based on total experience.
	public static double getLevelFromExp(long exp) 
	{
		if (exp > 1395)
		{
			return (Math.sqrt(72 * exp - 54215) + 325) / 18;
		}
		if (exp > 315) 
		{
			return Math.sqrt(40 * exp - 7839) / 10 + 8.1;
		}
		if (exp > 0) 
		{
			return Math.sqrt(exp + 9) - 3;
		}
		return 0;
	}

	/**
	 * @see http://minecraft.gamepedia.com/Experience#Leveling_up
	 * 
	 * "The formulas for figuring out how many experience orbs you need to get to the next level are as follows:
	 *  Experience Required = 2[Current Level] + 7 (at levels 0-15)
	 *                        5[Current Level] - 38 (at levels 16-30)
	 *                        9[Current Level] - 158 (at level 31+)"
	 */
	private static int getExpToNext(int level) {
		if (level > 30) 
		{
			return 9 * level - 158;
		}
		if (level > 15) 
		{
			return 5 * level - 38;
		}
		return 2 * level + 7;
	}

	// Give or take EXP
	public static void changePlayerExp(Player player, int exp)
	{
		exp += getExp(player);

		if (exp < 0) {
			exp = 0;
		}

		double levelAndExp = getLevelFromExp(exp);

		int level = (int) levelAndExp;
		player.setLevel(level);
		player.setExp((float) (levelAndExp - level));		
	}
	  
	public static ItemStack makeBottle(int xp) 
	{
		ItemStack bottle = new ItemStack(Material.POTION);
		PotionMeta met = (PotionMeta)bottle.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>(2);
		lore.add(ChatColor.YELLOW.toString()+xp+ChatColor.YELLOW+" orbs");
		DecimalFormat df = new DecimalFormat("0.0");
		lore.add(ChatColor.GRAY.toString() + "0-"+ df.format(getLevelFromExp((xp))));
		met.setLore(lore);
		if(xp < 550) 
		{
			met.setColor(Color.fromRGB(92, 198, 255));
			met.setDisplayName(ChatColor.BLUE+ "Bottle o' XP");
		}
		else if (xp < 1395) 
		{
			met.setColor(Color.fromRGB(109,255,43));
			met.setDisplayName(ChatColor.GREEN+ "Bottle o' XP");
		}
		else if (xp < 2920) 
		{
			met.setColor(Color.fromRGB(194,0,180));
			met.setDisplayName(ChatColor.DARK_PURPLE+ "Bottle o' XP");
		}
		else 
		{
			met.setColor(Color.fromRGB(140, 0, 0));
			met.setDisplayName(ChatColor.DARK_RED+ "Bottle o' XP");
		}
		met.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		bottle.setItemMeta(met);
		return bottle;
	}
	
	public static ItemStack makeLevelSplashPotion(int inputLevels, Player inputPlayer) 
	{
		int playerLevel = XPUtils.getLevel(inputPlayer);
		int inputLevel = inputLevels;
		
		int experierceToBottle = 0;
								
		if (inputLevels > 0)
		{
			int desiredLevel = playerLevel - inputLevel;
			for(int i = desiredLevel; i < playerLevel; i++)
			{
				experierceToBottle = experierceToBottle + getExpToNext(i);
			}			
			ItemStack bottle = new ItemStack(Material.SPLASH_POTION);
			PotionMeta met = (PotionMeta)bottle.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>(1);
			lore.add(ChatColor.LIGHT_PURPLE + (experierceToBottle + " orbs"));
			met.setLore(lore);
			met.setColor(Color.LIME);
			met.setDisplayName(ChatColor.DARK_AQUA.toString() + ChatColor.ITALIC +"Bottle o' XP");
			met.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
			met.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			met.addEnchant(Enchantment.LUCK, 1, false);
			bottle.setItemMeta(met);
			inputPlayer.setLevel(playerLevel - inputLevel);
			return bottle;
		}
		else
		{
			ItemStack bottle = new ItemStack(Material.GLASS_BOTTLE);
			return bottle;
		}
	}	
		
	public static ItemStack makeLevelPotion(int inputLevels) 
	{
		if (inputLevels > 0)
		{
			ItemStack bottle = new ItemStack(Material.POTION);
			PotionMeta met = (PotionMeta)bottle.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>(1);
			lore.add(ChatColor.LIGHT_PURPLE + (inputLevels + " levels"));
			met.setLore(lore);
			met.setColor(Color.LIME);
			met.setDisplayName(ChatColor.DARK_AQUA.toString() + ChatColor.ITALIC +"Bottle o' Levels");
			met.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
			bottle.setItemMeta(met);
			return bottle;
		}
		else
		{
			ItemStack bottle = new ItemStack(Material.GLASS_BOTTLE);
			return bottle;
		}
	}	
}
