package plugins.cardcraft.dev.xpplus.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import plugins.cardcraft.dev.xpplus.util.Utils;


public class XPPlusCommandTabCompleter implements TabCompleter{
	 @Override
	 public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
	   List<String> commandOptions = new ArrayList<String>();
	   
//	   for(int i = 0; i < args.length; i++)
//	   {
//		   Utils.debugLog("ARGS["+i+"]:"+args[i]);
//	   }
	   
	   if (args.length == 1) 
	   {
		   commandOptions.add("enable");
		   commandOptions.add("disable");
		   commandOptions.add("status");
	       return commandOptions;
	   }
	   else if(args.length == 2) 
	   {
	        Collection<?> playerCollection = Bukkit.getOnlinePlayers();
	        Iterator<?> playerIterator = playerCollection.iterator();
	        while(playerIterator.hasNext())
	        {
	        	Player player = (Player) playerIterator.next();
	        	commandOptions.add(player.getName());
	       	}
	        return commandOptions;
	   }

	   return new ArrayList<String>();
	 }

}
