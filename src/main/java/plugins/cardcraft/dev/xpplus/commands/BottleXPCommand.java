package plugins.cardcraft.dev.xpplus.commands;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import plugins.cardcraft.dev.xpplus.XPPlusPlugin;
import plugins.cardcraft.dev.xpplus.util.Utils;
import plugins.cardcraft.dev.xpplus.util.XPUtils;


public class BottleXPCommand implements CommandExecutor
{
	
    private XPPlusPlugin plugin;
	
    public BottleXPCommand(XPPlusPlugin plugin) {
        this.plugin = plugin;
    }
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) 
	{		
		if(!(sender instanceof Player)) return true;
		Player player = (Player)sender;
		
		for(int i = 0; i < args.length; i++)
		{
			Utils.debugLog("XP COMMAND ARG["+i+"]: "+args[i]);
		}
		

		//checks if their inventory is full
		if(player.getInventory().firstEmpty() == -1) 
		{
			Utils.sendMessage(player, ChatColor.DARK_AQUA + "[XPPLUS] " + ChatColor.BLUE + "Your inventory is full!");
			return true;
		}	
		else if(args != null && args.length == 1) 
		{
			try 
			{
				int playerLevel = XPUtils.getLevel(player);
				int inputLevel = Integer.parseInt(args[0]);
				
				if(inputLevel <= 0)
				{
					Utils.sendMessage(player, ChatColor.DARK_AQUA + "[XPPLUS] "+ ChatColor.RED + "Really???");
					return true;
				}
				
				if(playerLevel >= inputLevel)
				{
					player.getInventory().addItem(XPUtils.makeLevelSplashPotion(inputLevel,player));
					player.playSound(player.getLocation(), Sound.ENTITY_EVOKER_CAST_SPELL, 0.5f, 1.65f);					
					return true;
				}
				else
				{
					int currentLevel = player.getLevel();
					if(currentLevel <= 0)
					{
						Utils.sendMessage(player, ChatColor.DARK_AQUA + "[XPPLUS] "+ ChatColor.RED + "Really???");
						return true;
					}
					else
					{
						player.getInventory().addItem(XPUtils.makeLevelSplashPotion(currentLevel,player));
						Utils.sendMessage(player, ChatColor.DARK_AQUA + "[XPPLUS] "+ ChatColor.RED + "Not Enough Levels, Only Bottling "+currentLevel);
						player.playSound(player.getLocation(), Sound.ENTITY_EVOKER_CAST_SPELL, 0.5f, 1.65f);	
						return true;
					}
				}
			}
			catch(NumberFormatException e) 
			{
				Utils.sendMessage(player, ChatColor.DARK_AQUA + "[XPPLUS] "+ ChatColor.RED + "Invalid Number");
				return true;
			}
		}
		
		//returns false if they don't enter their arguments correctly
		else {
			return true;
		}
	}
	
}
