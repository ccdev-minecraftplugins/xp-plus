package plugins.cardcraft.dev.xpplus.listeners;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.GlowSquid;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;

import plugins.cardcraft.dev.xpplus.XPPlusPlugin;
import plugins.cardcraft.dev.xpplus.util.Logger;
import plugins.cardcraft.dev.xpplus.util.Utils;
import plugins.cardcraft.dev.xpplus.util.XPUtils;

public class XPPlusListener implements Listener{
	
	private XPPlusPlugin plugin;
	private World world;

	public XPPlusListener(XPPlusPlugin plugin) {
		this.plugin = plugin;
		this.world = Bukkit.getWorld("world");
	}
	
    @EventHandler
    public void dropXPonDeath(PlayerDeathEvent e)
    {
    	Player player = e.getEntity();
    	int total = XPUtils.getExp(player);
    	Logger.info("PLAYER: "+player.getName()+" XP DROP OF: "+total);
    	player.getWorld().spawn(player.getLocation(), ExperienceOrb.class, x -> x.setExperience(total));
    	
    }
    
    //Handling the Splash XP Potion
    @EventHandler
    public void splashPotion(PotionSplashEvent event)
    {
   		List<String> lore = event.getPotion().getItem().getItemMeta().getLore();
   		Location location;
   		
   		if(event.getHitEntity() != null)
   		{
   			location = event.getHitEntity().getLocation();
   		}
   		else if(event.getHitBlock() != null)
   		{
   			location = event.getHitEntity().getLocation();
   		}
   		else
   		{
   			location = event.getEntity().getLocation();
   		}
		   		
		if(lore!=null && lore.size()>0) 
		{
			if(lore.get(0).contains(" orbs")) 
			{
				Utils.debugLog("DETECTED SPLASH XP POTION OF ORBS");
				String xp = ChatColor.stripColor(lore.get(0));
				try 
				{
					int orbs = Integer.parseInt(xp.substring(0, xp.indexOf(' ')));
					world.spawn(location, ExperienceOrb.class, x -> x.setExperience(orbs));
				}
				catch(NumberFormatException e) 
				{
					return;
				}
			}		
		}    	
    }
    
    //Used to Drink XP Potion Bottles, Not Functional Yet
	@EventHandler
	public void drinkPotion(PlayerItemConsumeEvent event) 
	{
		if(event.getItem().getType().equals(Material.POTION)) 
		{
			List<String> lore = event.getItem().getItemMeta().getLore();
			if(lore!=null && lore.size()>0) 
			{
				if(lore.get(0).contains(" orbs")) 
				{
					Player player = event.getPlayer();
					String xp = ChatColor.stripColor(lore.get(0));
					int orbs;
					try 
					{
						orbs = Integer.parseInt(xp.substring(0, xp.indexOf(' ')));
					}
					catch(NumberFormatException e) 
					{
						return;
					}

					XPUtils.changePlayerExp(player, orbs);
					player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 0.5f, 1.5f);
					event.setCancelled(true);
					if(player.getInventory().getItemInMainHand().equals(event.getItem()))
					{
						player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
					}
					else
					{
						player.getInventory().setItemInOffHand(new ItemStack(Material.AIR));
					}

				}
				else if(lore.get(0).contains(" levels")) 
				{
					Player player = event.getPlayer();
					String xp = ChatColor.stripColor(lore.get(0));
					int levels;
					try 
					{
						levels = Integer.parseInt(xp.substring(0, xp.indexOf(' ')));
					}
					catch(NumberFormatException e) 
					{
						return;
					}
					
					if(!player.hasPermission("dropxp.drink")) 
					{
						player.sendMessage(ChatColor.DARK_AQUA + "[DropXP] " + ChatColor.RED + "You do not have permission to drink this potion");
						event.setCancelled(true);
						return;
					}
					player.setLevel(player.getLevel()+levels);
					player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 0.5f, 1.25f);
					event.setCancelled(true);
					if(player.getInventory().getItemInMainHand().equals(event.getItem()))
					{
						player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
					}
					else
					{	
						player.getInventory().setItemInOffHand(new ItemStack(Material.AIR));
					}
				}
				
			}
		}
	}    
}
